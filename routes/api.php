<?php

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//  Route::resource('user/','App\Http\Controllers\UsersController');
Route::post('user/login','App\Http\Controllers\usersController@login');     // Con token
Route::post('user/store','App\Http\Controllers\usersController@store');     // Con token

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('user/show/{id}','App\Http\Controllers\usersController@show');
    Route::post('user/update/{id}','App\Http\Controllers\usersController@update'); 
    Route::get('userVideo/{user_id}','App\Http\Controllers\UsersController@userVideo');
    Route::get('userComment/{user_id}','App\Http\Controllers\UsersController@userComment');
    Route::get('user/index','App\Http\Controllers\usersController@index')->middleware('admin');
    Route::delete('user/destroy/{id}','App\Http\Controllers\UsersController@destroy')->middleware('admin');;
});
  

//  Route::resource('comentarios/','App\Http\Controllers\ComentariosController');
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('comentarios/store','App\Http\Controllers\ComentariosController@store');
    Route::get('comentarios/show/{id}','App\Http\Controllers\ComentariosController@show');
    Route::post('comentarios/update/{id}','App\Http\Controllers\ComentariosController@update');
    Route::get('commentUser/{id}','App\Http\Controllers\ComentariosController@commentUser');
    Route::get('commentVideo/{id}','App\Http\Controllers\ComentariosController@commentVideo');
    Route::get('comentarios/index','App\Http\Controllers\ComentariosController@index');
    Route::delete('comentarios/destroy/{id}','App\Http\Controllers\ComentariosController@destroy')->middleware('admin');
});

//  Route::resource('/','App\Http\Controllers\VideosController');
Route::get('index','App\Http\Controllers\VideosController@index');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('store','App\Http\Controllers\VideosController@store');
    Route::get('show/{id}','App\Http\Controllers\VideosController@show');
    Route::post('update/{id}','App\Http\Controllers\VideosController@update');
    Route::get('videoUser/{id}','App\Http\Controllers\VideosController@videoUser');
    Route::get('videoComment/{video_id}','App\Http\Controllers\VideosController@videoComment');
    Route::delete('destroy/{id}','App\Http\Controllers\VideosController@destroy')->middleware('admin'); 
});

    //otro

    Route::get('user/logout','App\Http\Controllers\usersController@logout');