<?php

namespace App\Http\Controllers;

use Laravel\Passport\Passport;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Comentario;
use App\Models\User;
use App\Models\Video;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $user=User::all();
            return response()->json(['message'=>$user, 'status'=>'Success','code'=>200]);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid Data','code'=>400]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData=$request->validate([
            'role'=>'required|max:7',
            'name'=>'required|max:70',
            'surname'=>'required|max:70',
            'email'=>'email|required',
            'password'=>'required|confirmed',
            'image'=>'required'
            ]);

        $validateData['password']=bcrypt($request->password);

        try {
            $user=new User;

            $image=$request->file('image');
            $path=$image->store('public');

            $user->role=$request->input('role');
            $user->name=$request->input('name');
            $user->surname=$request->input('surname');
            $user->email=$request->input('email');
            $user->password=bcrypt($request->input('password'));
            $user->image=$image;
            $user->remember_token=$request['remember_token']=Str::random(10);
            $accessToken=$user->createToken('authToken')->accessToken;
            $user->save();

            return response(['message'=>$user, 'status'=>'Success','code'=>200, 'access_token'=>$accessToken]);

            // return response()->json(['message'=>'Register User Success','code'=>200]);

        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid or Data Exist','code'=>400]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $user=User::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid ID or No record Data','code'=>400]);
        }
    }  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user=User::findOrFail($id);
            
            $image=$request->file('image');
            $path=$image->store('public');

            $user->role=$request->input('role');
            $user->name=$request->input('name');
            $user->surname=$request->input('surname');
            $user->email=$request->input('email');
            $user->password=bcrypt($request->input('password'));
            $user->image=$image;
            $user->save();
            return response()->json(['message'=>'Register Updated Success','code'=>200]);

        } catch (\Exception $e) {
            return response()->json(['message'=>'Error Updated User Data','code'=>400]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user=User::findOrFail($id);
            $user->delete();
            return response()->json(['message'=>'Register Deleted Success','code'=>200]);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Not possible deleted User Id','code'=>400]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    } 

    /**
     * Relation 1-N User->Video. ( All videos of user with user_id )
     *
     * @param  int  $user_id
     * @return \Illuminate\Http\Response
     */
     public function userVideo($user_id)
     {
        $videos=User::find($user_id)->videos;
        return response()->json($videos);
     }

    /**
     * Relation 1-N User->Comment. ( All comments of user with user_id )
     *
     * @param  int  $user_id
     * @return \Illuminate\Http\Response
     */
    public function userComment($user_id)
    {
        $comentarios=User::find($user_id)->comentarios;
        return response()->json($comentarios); 
    }

    /**
     * Login in App ( token Bearer )
     *
     * @param  Bearer
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request){
        try {
            $loginData=$request->validate([
                'email'=>'email|required',
                'password'=>'required'
            ]);

            if(!auth()->attempt($loginData))
                return response()->json(['message'=>'Invalid Credentials Unauthorized!!!!!','code'=>400]);
            
            $accessToken=auth()->user()->createToken('authToken')->accessToken;
                return response(['user'=>auth()->user(), 'status'=>'Login Success!!!!!','code'=>200, 'access_token'=>$accessToken]);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Error!! Invalid Credentials Data!!!!!','code'=>400]);
        }
    }

    /**
     * Login out App
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Bearer
     * @return \Illuminate\Http\Response
     */

    public function logout (Request $request) {
        try {
            $token = $request->user()->token();
            $token->revoke();
            return response()->json(['message'=>'User Log Out, Bye!!!!!','code'=>200]);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Error for User Log Out!!!!!','code'=>400]);
        }
    }
}