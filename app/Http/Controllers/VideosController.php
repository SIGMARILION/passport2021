<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Comentario;
use App\Models\User;
use App\Models\Video;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $video=Video::all();
            return response()->json(['message'=>$video, 'status'=>'Success','code'=>200]);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid Data','code'=>400]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData=$request->validate([
            'user_id'=>'required|max:7',
            'title'=>'required|max:50',
            'description'=>'required|max:50',
            'status'=>'required|max:1',
            'image'=>'required|max:60'
            ]);

        try {
            $video=new Video;

            $image=$request->file('image');
            $path=$image->store('public');

            $video->user_id=$request->input('user_id');
            $video->title=$request->input('title');
            $video->description=$request->input('description');
            $video->status=$request->input('status');
            $video->image=$image;
            $video->video_path=Storage::url($path);
            $video->save();

            return response()->json(['message'=>'Register Video Success','code'=>200]);

        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid or Video Data Exist','code'=>400]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $video=Video::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid Video ID or No record Data','code'=>400]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $video=Video::findOrFail($id);            

            $image=$request->file('image');
            $path=$image->store('public');

            $video->user_id=$request->input('user_id');
            $video->title=$request->input('title');
            $video->description=$request->input('description');
            $video->status=$request->input('status');
            $video->image=$image;
            $video->video_path=Storage::url($path);
            $video->save();

            return response()->json(['message'=>'Video Updated Success','code'=>200]);

        } catch (\Exception $e) {
            return response()->json(['message'=>'Error Updated Video Data','code'=>400]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $video=Video::findOrFail($id);
            $video->delete();
            return response()->json(['message'=>'Video Deleted Success','code'=>200]);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Not possible deleted Video Id','code'=>400]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Relation 1-N Video->Coments. ( All comments in a video with video_id )
     *
     * @param  int  $video_id
     * @return \Illuminate\Http\Response
     */
     public function videoComment($video_id)
     {
        $comentarios=Video::find($video_id)->comentarios;
        return response()->json($comentarios);
     }

    /**
     * Relation 1-1 Video->User. ( Unic user in a video with id of video )
     *
     * @param  int  $video_id
     * @return \Illuminate\Http\Response
     */
    public function videoUser($video_id)
    {
        $userId=Video::find($video_id)->user_id;                
        $user=User::find($userId);
        return response()->json($user);
    }
}