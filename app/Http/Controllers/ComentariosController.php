<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Comentario;
use App\Models\User;
use App\Models\Video;

class ComentariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $comentario=Comentario::all();
            return response()->json(['message'=>$comentario, 'status'=>'Success','code'=>200]);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid Data','code'=>400]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData=$request->validate([
            'user_id'=>'required|max:7',
            'video_id'=>'required|max:7',
            'body'=>'required|max:50'
            ]);

        try {
            $comentario=new Comentario;
            $comentario->user_id=$request->input('user_id');
            $comentario->video_id=$request->input('video_id');
            $comentario->body=$request->input('body');
            $comentario->save();

            return response()->json(['message'=>'Register Comment Success','code'=>200]);

        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid or Comment Data Exist','code'=>400]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $comentario=Comentario::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Invalid Comment ID or No record Data','code'=>400]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $comentario=Comentario::findOrFail($id);
            $comentario->user_id=$request->input('user_id');
            $comentario->video_id=$request->input('video_id');
            $comentario->body=$request->input('body');            
            $comentario->save();
            return response()->json(['message'=>'Comment Updated Success','code'=>200]);

        } catch (\Exception $e) {
            return response()->json(['message'=>'Error Updated Comment Data','code'=>400]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $comentario=Comentario::findOrFail($id);
            $comentario->delete();
            return response()->json(['message'=>'Comment Deleted Success','code'=>200]);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Not possible deleted Comment Id','code'=>400]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Relation 1-1 Comment->User. ( Unic comment of user with id of comment )
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function commentUser($id)
     {
        $userId=Comentario::find($id)->user_id;                
        $user=User::find($userId);
        return response()->json($user);
     }

     /**
     * Relation 1-1 Comment->Video. ( Unic comment in a video with video id )
     *
     * @param  int  $video_id
     * @return \Illuminate\Http\Response
     */
    public function commentVideo($id)
    {
        $videoId=Comentario::find($id)->video_id;                
        $video=Video::find($videoId);
        return response()->json($video);
    }
}