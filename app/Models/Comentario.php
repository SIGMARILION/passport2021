<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Comentario extends Model
{
    use HasFactory, HasApiTokens;

    public function usuario(){ // 1-1
        return $this->belongsTo('App\Models\User');
    } 

    public function video(){ // 1-1
        return $this->belongsTo('App\Models\Video');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['user_id','video_id','body'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at','updated_at','deleted_at'];
}